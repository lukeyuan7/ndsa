import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.io.PrintWriter;


import javax.swing.JOptionPane;

public class Database 
{
	/** Database Connection */
    private Connection con;
    
    /** Used for executing database statements*/
    private Statement stmt;      
    
    /** Holds result of stmt, moves to next row in database*/
    private ResultSet rs;
    
    /** Holds the number of rows in database value*/
    int count;
    
    /** Holds the current number of the row in the database we are on*/
    int current;
	
	
	private String cus_firstName;
	private String cus_lastName;
	private String cus_address;
	private String cus_email;
	private String cus_phone;
	private String cus_id;
	private String cus_Year;
	private String cus_area;
	
	private String pub_Name;
	private String pub_Category;
	private String pub_Release;
	private String pub_id;
	
	private String pubId;
	private String hStart;
	private String hEnd;
	private String sub_Id;
	
	private GUI gui;
	
    public Database()
    {
        con = null;         /** @param con       the connection parameter to connect to MS Access database.*/
        stmt = null;        /** @param stmt      the statement parameter to execute database statements.*/
        rs = null;          /** @param rs        the result parameter to hold the result of statement and move rows.*/
        count = 0;          /** @param count     the int value to count number of rows in database.*/
        current = 0;        /** @param current   the int value to keep track of which row in database we are on.*/
    
        dbConn();// method to connect to database using odbc-jdbc
        initDB();// method to initialise gui with database info

    }
    public void connectToGui(GUI gui) {
    	this.gui = gui;
    }
    
    private void initDB() {
		// TODO Auto-generated method stub
		
	}

	public void dbConn()
    {
		try		
		{
				// driver to use with named database which is stored alongside program in C Drive
		        String url = "jdbc:ucanaccess://c:/BNotes/backEnd.accdb";
		        
		        // connection represents a session with a specific database
		        con = DriverManager.getConnection(url);
		
		        // stmt used for executing sql statements and obtaining results
		        stmt = con.createStatement();
		        System.out.println("Connected");
		        rs = stmt.executeQuery("SELECT * FROM Customer");
		
				while(rs.next())	// count number of rows in table
		        {
		            count++;
		        }
		        System.out.println(count);
		        rs.close();
		}
		catch(Exception e) 
		{
			System.out.println("Unable to connect to database");
			/*JOptionPane.showMessageDialog(database.this,"Error in startup.","Error", JOptionPane.PLAIN_MESSAGE);} */
		}		
    }


public void addNewCustomer(String firstName, String lastName,String YearOfBirth, String address, String email, String phone, String Area)
	//boolean cus=false;
	{
	try 
        {
		this.cus_Year = YearOfBirth;
		this.cus_firstName = firstName;
		this.cus_lastName = lastName;
		this.cus_address = address;
		this.cus_email = email;
		this.cus_phone = phone;
		this.cus_area = Area;
		
		
		
			String customer = "INSERT INTO Customer(FirstName, LastName, YearOfBirth, Address, Email, PhoneNumber, Area)VALUES('"+cus_firstName+"', '"+cus_lastName+"', '"+cus_Year+"', '"+cus_address+"', '"+cus_email+"', '"+cus_phone+"', '"+cus_area+"')";
			stmt.executeUpdate(customer);
			//count++;
			
			//String customerId = "Select count(*) from Customer";
			//stmt.executeUpdate(customerId);
			//String cusId = stmt.toString();
			//System.out.println(cusId);
			
			//String customerSId = "Select Max(Sub_Id) from Customer;";
			//stmt.executeUpdate(customerSId);
			//String cusSId = stmt.toString();
			//System.out.println(cusId + cusSId);
			//return true;
			//gui.createCustomer(cusId, cusSId);
		}
        catch (SQLException e) 
        {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//return false;
			
		}
 
	}

/*
public void updateCustomer(String id, String firstName, String lastName, String YearOfBirth, String address, String email, String phone, String Area)
//boolean cus=false;
{
try 
    {
	this.cus_area = Area;
	this.cus_id = id;
	this.cus_firstName = firstName;
	this.cus_lastName = lastName;
	this.cus_address = address;
	this.cus_email = email;
	this.cus_phone = phone;
	this.cus_Year = YearOfBirth;
	
	//cus_id = 26;
	
		String updateTemp = "UPDATE Customer SET "+"FirstName = '"+cus_firstName+"', LastName = '"+cus_lastName+"', Address = '"+cus_address+"', Email ='"+cus_email+"', PhoneNumber = '"+cus_phone+"' where Cus_Id = "+cus_id;
		
		stmt.execute(updateTemp);
		System.out.println("Customer has been altered");
		
	}
    catch (SQLException e) 
    {
		// TODO Auto-generated catch block
		e.printStackTrace();
		
		
	}

}


public boolean deleteCustomer(String id)
//boolean cus=false;
{
try 
  {
	this.cus_id = id;

	
		String deleteTemp = "DELETE From Customer where Cus_Id = "+cus_id;
		
		stmt.executeUpdate(deleteTemp);
		count--;
		System.out.println("Customer has been deleted");
		return true;
	}
  catch (SQLException e) 
  {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return false;
		
	}

}


public void searchCustomer(String nameToFind)
{	
	int foundCus = 0;
	boolean found=false;
		
try	
{
	
	rs=stmt.executeQuery("SELECT * FROM Customer Where LastName = '"+nameToFind+"'");

	while(rs.next()&& found == false)
	{
		foundCus++;
		String CusId = rs.getString("Cus_Id");
		String CusName = rs.getString("FirstName");
		String Surname = rs.getString("LastName");
		String CusAddress = rs.getString("Address");
		String CusEmail = rs.getString("Email");
		String CusPhoneNo = rs.getString("PhoneNumber");
		String SubId = rs.getString("Sub_Id");
		
		System.out.println(CusId);
		System.out.println(CusName);
		System.out.println(Surname);
		System.out.println(CusAddress);
		System.out.println(CusEmail);
		System.out.println(CusPhoneNo);
		System.out.println(SubId);
		CusName.trim();
		if(CusName.equals(nameToFind)) {
			
			//gui.appendCust();
			found = true;
		}
		
	}
	rs.close();
}
catch(Exception e) {System.out.println("Error in finding customer in database");}
}

public void searchCustomerId(String idToFind)
{	
	int foundCus = 0;
	boolean found=false;
		
try	
{
	
	rs=stmt.executeQuery("SELECT * FROM Customer Where Cus_Id = '"+idToFind+"'");

	while(rs.next()&& found == false)
	{
		foundCus++;
		String CusId = rs.getString("Cus_Id");
		String CusName = rs.getString("FirstName");
		String Surname = rs.getString("LastName");
		String CusAddress = rs.getString("Address");
		String CusEmail = rs.getString("Email");
		String CusPhoneNo = rs.getString("PhoneNumber");
		String SubId = rs.getString("Sub_Id");
		
		System.out.println(CusId);
		System.out.println(CusName);
		System.out.println(Surname);
		System.out.println(CusAddress);
		System.out.println(CusEmail);
		System.out.println(CusPhoneNo);
		System.out.println(SubId);
		CusName.trim();
		if(CusId.equals(idToFind)) {
			
			//gui.appendCust();
			found = true;
		}
		
	}
	rs.close();
}
catch(Exception e) {System.out.println("Error in finding customer in database");}
}

public void searchCustomerAddress(String addressToFind)
{	
	int foundCus = 0;
	boolean found=false;
		
try	
{
	
	rs=stmt.executeQuery("SELECT * FROM Customer Where Cus_Id = '"+addressToFind+"'");

	while(rs.next()&& found == false)
	{
		foundCus++;
		String CusId = rs.getString("Cus_Id");
		String CusName = rs.getString("FirstName");
		String Surname = rs.getString("LastName");
		String CusAddress = rs.getString("Address");
		String CusEmail = rs.getString("Email");
		String CusPhoneNo = rs.getString("PhoneNumber");
		String SubId = rs.getString("Sub_Id");
		
		System.out.println(CusId);
		System.out.println(CusName);
		System.out.println(Surname);
		System.out.println(CusAddress);
		System.out.println(CusEmail);
		System.out.println(CusPhoneNo);
		System.out.println(SubId);
		CusName.trim();
		if(CusAddress.equals(addressToFind)) {
			
			//gui.appendCust();
			found = true;
		}
		
	}
	rs.close();
}
catch(Exception e) {System.out.println("Error in finding customer in database");}
}

public void searchCustomerSubId(String SubIdToFind)
{	
	int foundCus = 0;
	boolean found=false;
		
try	
{
	
	rs=stmt.executeQuery("SELECT * FROM Customer Where Sub_Id = '"+SubIdToFind+"'");

	while(rs.next()&& found == false)
	{
		foundCus++;
		String CusId = rs.getString("Cus_Id");
		String CusName = rs.getString("FirstName");
		String Surname = rs.getString("LastName");
		String CusAddress = rs.getString("Address");
		String CusEmail = rs.getString("Email");
		String CusPhoneNo = rs.getString("PhoneNumber");
		String SubId = rs.getString("Sub_Id");
		
		
		System.out.println(CusId);
		System.out.println(CusName);
		System.out.println(Surname);
		System.out.println(CusAddress);
		System.out.println(CusEmail);
		System.out.println(CusPhoneNo);
		System.out.println(SubId);
		//CusName.trim();
		if(SubId.equals(SubIdToFind)) {
			
			//gui.appendCust();
			found = true;
		}
		
	}
	rs.close();
}
catch(Exception e) {System.out.println("Error in finding customer in database");}
}

public boolean addNewSubscription(String pubId, String hStart, String hEnd){
try 
    {
	this.pubId = pubId;
	this.hStart= hStart;
	this.hEnd= hEnd;
	
		String subscription = "INSERT INTO Subscription(Pub_Id, Holiday_Start, Holiday_Ends)VALUES('"+pubId+"', '"+hStart+"', '"+hEnd+"')";
		stmt.executeUpdate(subscription);
		count++;
		System.out.println("New subscription has been added");
		 return true;
	}
    catch (SQLException e) 
    {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return false;
		
	}
}


public boolean updateSuscription(String id,String pubId, String hStart, String hEnd){
try 
    {
	this.sub_Id=id;
	this.pubId = pubId;
	this.hStart= hStart;
	this.hEnd= hEnd;
		String updateTemp = "UPDATE Subscription SET " + "PubName = '"+pubId+"', Holiday_Start = '"+hStart+"', Holiday_Ends = '"+hEnd+"' where Sub_Id = "+sub_Id;
	
		stmt.execute(updateTemp);
		System.out.println("Subscription has been altered");
		return true;
	}
  catch (SQLException e) 
  {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return false;
		
	}

}

public boolean deleteSubscribtion(String id)
//boolean cus=false;
{
try 
{
	this.sub_Id = id;

	
		String updateTemp = "DELETE from Subscription where Sub_Id = "+sub_Id;
	
		stmt.executeUpdate(updateTemp);
		count--;
		System.out.println("Subscription has been deleted");
		return true;
	}
catch (SQLException e) 
{
		// TODO Auto-generated catch block
		e.printStackTrace();
		return false;
		
	}

}

public void searchPublicationId(String idToFind)
{	
	int foundPub = 0;
	boolean found=false;
		
try	
{
	
	rs=stmt.executeQuery("SELECT * FROM Publication Where Pud_Id = '"+idToFind+"'");

	while(rs.next()&& found == false)
	{
		foundPub++;
		String pubId = rs.getString("Pub_Id");
		String pubName = rs.getString("PubName");
		String pubCat = rs.getString("PubCategory");
		String publish = rs.getString("Published");
		
		
		System.out.println(pubId);
		System.out.println(pubName);
		System.out.println(pubCat);
		System.out.println(publish);

		//pubId.trim();
		if(pubId.equals(idToFind)) {
			
			//gui.appendCust();
			found = true;
		}
		
	}
	rs.close();
}
catch(Exception e) {System.out.println("Error in finding Publication in database");}
}


public void searchPublicationName(String nameToFind)
{	
	int foundPub = 0;
	boolean found=false;
		
try	
{
	
	rs=stmt.executeQuery("SELECT * FROM Publication Where PubName = '"+nameToFind+"'");

	while(rs.next()&& found == false)
	{
		foundPub++;
		String pubId = rs.getString("Pub_Id");
		String pubName = rs.getString("PubName");
		String pubCat = rs.getString("PubCategory");
		String publish = rs.getString("Published");
		
		
		System.out.println(pubId);
		System.out.println(pubName);
		System.out.println(pubCat);
		System.out.println(publish);

		//pubId.trim();
		if(pubId.equals(nameToFind)) {
			
			//gui.appendCust();
			found = true;
		}
		
	}
	rs.close();
}
catch(Exception e) {System.out.println("Error in finding Publication in database");}
}

public boolean addNewPublication(String name, String category, String release){
try 
    {
	this.pub_Name = name;
	this.pub_Category= category;
	this.pub_Release= release;
	
		String publication = "INSERT INTO Publication(PubName, PubCategory, Release)VALUES('"+pub_Name+"', '"+pub_Category+"', '"+pub_Release+"')";
		stmt.executeUpdate(publication);
		count++;
		System.out.println("New publication has been added");
		 return true;
	}
    catch (SQLException e) 
    {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return false;
		
	}
}


public boolean updatePublication(String id, String name, String category, String release)
//boolean cus=false;
{
try 
  {
	this.pub_id = id;
	this.pub_Name = name;
	this.pub_Category= category;
	this.pub_Release= release;
	
		String updateTemp = "UPDATE Publication SET " + "PubName = '"+pub_Name+"', PubCategory = '"+pub_Category+"', Release = '"+pub_Release+"' where Pub_Id = "+pub_id;
	
		stmt.execute(updateTemp);
		System.out.println("Publication has been altered");
		return true;
	}
  catch (SQLException e) 
  {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return false;
		
	}

}

public boolean deletePublication(String id, String name, String category, String release)
//boolean cus=false;
{
try 
{
	this.pub_id = id;
	this.pub_Name = name;
	this.pub_Category= category;
	this.pub_Release= release;
	
		String updateTemp = "DELETE from Publication where Pub_Id = "+pub_id;
	
		stmt.executeUpdate(updateTemp);
		count--;
		System.out.println("Publication has been deleted");
		return true;
	}
catch (SQLException e) 
{
		// TODO Auto-generated catch block
		e.printStackTrace();
		return false;
		
	}

}*/


}
